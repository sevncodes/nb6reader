import re
import numpy as np
import pandas as pd


class NB6LogReader:

    def __init__(self, log, identifier=None):

        self.log = log
        self.matchnum="[+|-]?[0-9]+\.?[0-9]*(?:e|E)?[+|-]?[0-9]*|nan"
        if identifier is None:
            self.identifier = log
        else:
            self.identifier = identifier

    def add_identifier(func):
        def _add_identifier(self):
            df = func(self)
            df["fileid"] = self.identifier
            return df

        return _add_identifier

    @add_identifier
    def getCE(self):
        matchnum = self.matchnum
        storenum = f"({matchnum})"

        CEmatchpattern = "(?i)BINARY\s+CE.*A\s+" + f"{storenum}\s+" * 14 + storenum

        with open(self.log, "r") as fo:
            i = fo.read()
            ma = re.findall(CEmatchpattern, i)
            na = np.array(ma, dtype=float)

            names = ("ID1", "ID2", "K10", "K20", "K1", "K2", "M1", "M2", "DM", "E0", "E", "R1", "R2", "A0", "A")

            if len(na)==0:
                df = pd.DataFrame({names[i]: [] for i in range(len(names))})
            else:
                df = pd.DataFrame({names[i]: na[:, i] for i in range(len(names))})

            df = df.astype(
                {"ID1": "int64", "ID2": "int64", "K1": "int64", "K10": "int64", "K2": "int64", "K20": "int64"}, )
        return df

    @add_identifier
    def getBinaryKick(self):
        matchnum = self.matchnum
        storenum = f"({matchnum})"

        matchpattern = "(?i)BINARY\s+KICK:.*R12\[NB\]\s+" + f"{storenum}\s+" * 10 + storenum

        with open(self.log, "r") as fo:
            i = fo.read()
            ma = re.findall(matchpattern, i)
            na = np.array(ma, dtype=float)

            names = ("ID1", "ID2", "K1", "K2", "M1", "M2", "VESC", "VDIS", "R12_SEMI", "EB", "R12_NB")

            if len(na)==0:
                df = pd.DataFrame({names[i]: [] for i in range(len(names))})
            else:
                df = pd.DataFrame({names[i]: na[:, i] for i in range(len(names))})

            df = df.astype({"ID1": "int64", "ID2": "int64", "K1": "int64", "K2": "int64"}, )
        return df

    @add_identifier
    def getKick(self):
        matchnum = self.matchnum
        storenum = f"({matchnum})"

        vallist = ("I", "NAME", "Time\[Myr\]", "K\*0", "K\*", "K\*\(ICM\)", "M0\[M\*\]",
                   "MN\[M\*\]", "VI\[km\/s\]", "VK\[km\/s\]", "VF\[km\/s\]", "VK0\[km\/s\]",
                   "FB")

        matchpattern = "(?i)VELOCITY\s+KICK:"
        for val in vallist:
            matchpattern += f"\s+{val}\s+{storenum}"

        with open(self.log, "r") as fo:
            i = fo.read()
            ma = re.findall(matchpattern, i)
            na = np.array(ma, dtype=float)

            names = ("I", "ID", "Time", "K0", "K", "K_ICM", "M0", "MREM", "VI", "VK", "VF",
                     "VK0", "FB")

            if len(na)==0:
                df = pd.DataFrame({names[i]: [] for i in range(len(names))})
            else:
                df = pd.DataFrame({names[i]: na[:, i] for i in range(len(names))})

            df = df.astype({"I": "int64", "ID": "int64", "K0": "int64", "K": "int64"}, )
        return df

    @add_identifier
    def getNSBHbinary(self):

        matchnum = self.matchnum
        storenum = f"({matchnum})"

        matchpattern = "(?i)NS\/BH\s+BINARY\s+" + f"T\s+{storenum}\s+" + f"NM1,2,S\s+{storenum}\s+{storenum}\s+{storenum}\s+"
        matchpattern += f"KW1,2,S=\s+{storenum}\s+{storenum}\s+{storenum}\s+" + f"M1,2\[\*\]\s+{storenum}\s+{storenum}"

        with open(self.log, "r") as fo:
            i = fo.read()
            ma = re.findall(matchpattern, i)
            na = np.array(ma, dtype=float)
            names = ("T_NB", "ID1", "ID2", "IDS", "K1", "K2", "KS", "M1", "M2")


            if len(na)==0:
                df = pd.DataFrame({names[i]: [] for i in range(len(names))})
            else:
                df = pd.DataFrame({names[i]: na[:, i] for i in range(len(names))})

            df = df.astype(
                {"ID1": "int64", "ID2": "int64", "IDS": "int64", "K1": "int64", "K2": "int64", "KS": "int64"}, )
        return df

    @add_identifier
    def getCHAINCE(self):
        matchnum = self.matchnum
        nstorenum = f"(?:{matchnum})"
        storenum = f"({matchnum})"
        CEmatchpattern = "(?i)CHAIN\s+CE\s+NAM\s+K0\*\s+K\*\s+M\s+M2\s+DM\s+E\s+R1\s+R2\s+A0\s+A\s+EB\s+DP\s+"
        CEmatchpattern += f"{storenum}\s+{storenum}\s+{storenum}\s+{storenum}\s+" #NAME1 NAME2 NAME3  NAMEOUT
        CEmatchpattern += f"{storenum}\s+{storenum}\s+{storenum}\s+{storenum}\s+"  # K1 K2 K1OLD K2OLD
        CEmatchpattern += f"{storenum}\s+{storenum}\s+{storenum}\s+" #M1 M2 DM
        CEmatchpattern += f"{storenum}\s+{storenum}\s+{storenum}\s+"  #E R1 R2
        CEmatchpattern += f"{storenum}\s+{storenum}\s+"  # A0 A

        with open(self.log, "r") as fo:
            i = fo.read()
            ma = re.findall(CEmatchpattern, i)
            na = np.array(ma, dtype=float)

            names = ("ID1", "ID2", "ID3", "IDOUT", "K10", "K20", "K1", "K2", "M1", "M2", "DM",  "E", "R1", "R2", "A0", "A")

            if len(na)==0:
                df = pd.DataFrame({names[i]: [] for i in range(len(names))})
            else:
                df = pd.DataFrame({names[i]: na[:, i] for i in range(len(names))})

            df = df.astype(
                {"ID1": "int64", "ID2": "int64", "ID3":"int64", "IDOUT":"int64", "K1": "int64", "K10": "int64", "K2": "int64", "K20": "int64"},
            )
        return df


    @add_identifier
    def getKSREG(self):

        matchnum = self.matchnum
        nstorenum = f"(?:{matchnum})"
        storenum = f"({matchnum})"

        matchpattern = "(?i)NEW\s+KSREG"

        matchpattern += f"\s+TIME\[NB\]\s+{storenum}"
        matchpattern += f"\s+NM1,2,S=\s+{storenum}\s+{storenum}\s+{storenum}"
        matchpattern += f"\s+KW1,2,S=\s+{storenum}\s+{storenum}\s+{storenum}"
        matchpattern += f"\s+IPAIR\s+{storenum}"
        matchpattern += f"\s+DTAU\s+{nstorenum}"
        matchpattern += f"\s+M1,2\[NB\]\s+{nstorenum}\s+{nstorenum}"
        matchpattern += f"\s+R12\[NB\]\s+{nstorenum}"
        matchpattern += f"\s+e,a,eb\[NB\]=\s+{storenum}\s+{nstorenum}\s+{storenum}"
        matchpattern += f"\s+P\[d\]=\s+{storenum}"
        matchpattern += f".*M1,2\[\*\]\s+{storenum}\s+{storenum}"

        with open(self.log, "r") as fo:
            i = fo.read()
            ma = re.findall(matchpattern, i)
            na = np.array(ma, dtype=float)

            names = ("T_NB", "ID1", "ID2", "IDS", "K1", "K2", "KS", "IPAIR",
                     "E","EB","P","M1","M2")


            if len(na)==0:
                df = pd.DataFrame({names[i]: [] for i in range(len(names))})
            else:
                df = pd.DataFrame({names[i]: na[:, i] for i in range(len(names))})

            df = df.astype(
                {"ID1": "int64", "ID2": "int64", "IDS": "int64",
                 "K1": "int64", "K2": "int64", "KS": "int64",
                 "IPAIR":"int64"}
            )

        return df
