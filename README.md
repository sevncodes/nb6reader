# NB6reader

##Example 

- First of all import the log reader
    ```python 
    from nb6reader import NB6LogReader
   ```
- Then create the obeject and load the file, 
  assume that the file is called out and it is the same  path
    ```python 
    mo=NB6LogReader("out")
   ```
  By default all the dataframe will include an additional column 
  _identifer_ containing the name of the file. You can 
 use a custom value for this column, for example if Mcl is the 
 mass of the anlysed cluster 
  ```python 
    mo=NB6LogReader("out",identifier=Mcl)
   ```
- get the information 

    - information about CE events: 
    ```python 
        mo.getCE()
   ```
  - information about kicks in binaries: 
   ```python 
       mo.getBinaryKick()
  ```
  - information about kicks: 
   ```python 
       mo.getKick()
  ```
  - information about binary with a NS/BH: 
   ```python 
       mo.getNSBHbinary()
  ```

    All this methods return always a dataframe