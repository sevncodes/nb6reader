from __future__ import print_function
from setuptools import setup
from nb6reader import __version__
import sys

#Check version
if sys.version_info < (3,8):
    sys.exit('Sorry, Python < 3.8 is not supported')

setup(
    name='nb6reader',
    version=__version__,
    author='G. Iorio',
    author_email='giuliano.iorio.astro@gmail.com',
    packages=['nb6reader',],
    scripts=[],
    url='https://gitlab.com/sevncodes/nb6reader',
    license='',
    description='suit of utilities for analysing NB6 outputs',
    long_description=open('README.md').read(),
    install_requires=[
      "numpy", "pandas"
    ],
    include_package_data=True,
)
